﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UltimateGameTracker.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using UltimateGameTracker.Data.Entities;
using Microsoft.Extensions.Logging;

namespace UltimateGameTracker.Data
{
    public class UltimateGameTrackerContext : DbContext
    {
        private readonly IConfiguration _config;

        public UltimateGameTrackerContext(DbContextOptions options, IConfiguration config) : base(options)
        {
            _config = config;
        }

        public DbSet<Event> Events { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<GameType> GameTypes { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
#if DEBUG
            optionsBuilder.UseSqlServer(_config.GetConnectionString("UltimateGameTrackerLocal"));
#elif RELEASE
            optionsBuilder.UseSqlServer(_config.GetConnectionString("UltimateGameTrackerRelease"));
#endif
        }

        protected override void OnModelCreating(ModelBuilder bldr)
        {
            bldr.Entity<Event>()
                .HasData(new
                {
                    EventId = 1,
                    Name = "Ultimate Player's Pickup",
                    IsReoccuring = true,
                    DateTime = DateTime.Now,
                    LocationId = 1,
                    CreationDate = DateTime.Now,
                    ModifiedDate = DateTime.Now
                });

            bldr.Entity<GameType>()
                .HasData(new
                {
                    GameTypeId = 1,
                    Name = "Pickup",
                    CreationDate = DateTime.Now,
                    ModifiedDate = DateTime.Now
                },
                new
                {
                    GameTypeId = 2,
                    Name = "League",
                    CreationDate = DateTime.Now,
                    ModifiedDate = DateTime.Now
                });

            bldr.Entity<Game>()
                .HasData(new
                {
                    GameId = 1,
                    Name = "Aaron's Game",
                    Description = "Regular Pickup",
                    CreationDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    EventId = 1,
                    GameTypeId = 1
                });

            bldr.Entity<Location>()
                .HasData(new
                {
                    LocationId = 1,
                    Address = "123 Mockingbird Ln",
                    City = "Bountiful",
                    State = "UT",
                    Zip = "84010",
                    Country = "USA",
                    IsAltLocation = false,
                    CreationDate = DateTime.Now,
                    ModifiedDate = DateTime.Now
                });

            bldr.Entity<User>()
                .HasData(new
                {
                    UserId = 1,
                    FirstName = "Aaron",
                    LastName = "Salazar",
                    Email = "tyburn.jig@gmail.com",
                    UserName = "tyburn.jig@gmail.com",
                    Password = "n4bQgYhMfWWaL+qgxVrQFaO/TxsrC4Is0V1sFbDwCgg=", // "test"
                    Role = "admin",
                    CreationDate = DateTime.Now,
                    ModifiedDate = DateTime.Now
                });

            // Many to many with composit key
            bldr.Entity<UserGame>()
                .HasKey(ug => new
                {
                    ug.UserId,
                    ug.GameId
                });
            bldr.Entity<UserGame>()
                .HasData(new
                {
                    UserId = 1,
                    GameId = 1
                });
        }
    }
}