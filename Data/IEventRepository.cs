﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimateGameTracker.Data.Entities;

namespace UltimateGameTracker.Data
{
    public interface IEventRepository
    {
        // General 
        void Add<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveChangesAsync();

        Task<Event[]> GetAllEventsAsync();
        Task<Event> GetEventAsync(int eventId);
        Task<Event[]> GetAllEventsByDate(DateTime dateTime);
    }
}
