﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimateGameTracker.Data.Entities;

namespace UltimateGameTracker.Data
{
    public class EventRepository : BaseRepository, IEventRepository
    {
        private readonly UltimateGameTrackerContext _context;

        public EventRepository(UltimateGameTrackerContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Event[]> GetAllEventsAsync()
        {
            IQueryable<Event> query = _context.Events
                .Include(e => e.Games).ThenInclude(g => g.GameType)
                .Include(e => e.Location);

            query = query.OrderByDescending(c => c.DateTime);

            return await query.ToArrayAsync();
        }

        public async Task<Event[]> GetAllEventsByDate(DateTime dateTime)
        {
            IQueryable<Event> query = _context.Events
                .Where(e => e.DateTime.Date == dateTime.Date)
                .Include(e => e.Games).ThenInclude(g => g.GameType)
                .Include(e => e.Location)
                .OrderByDescending(e => e.DateTime);

            return await query.ToArrayAsync();
        }

        public async Task<Event> GetEventAsync(int eventId)
        {
            IQueryable<Event> query = _context.Events
                .Where(e => e.EventId == eventId)
                .Include(e => e.Games).ThenInclude(g => g.GameType)
                .Include(e => e.Location);

            return await query.FirstOrDefaultAsync();
        }
    }
}
