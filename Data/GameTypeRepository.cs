﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimateGameTracker.Data.Entities;

namespace UltimateGameTracker.Data
{
    public class GameTypeRepository : BaseRepository, IGameTypeRepository
    {
        private readonly UltimateGameTrackerContext _context;

        public GameTypeRepository(UltimateGameTrackerContext context) : base(context)
        {
            _context = context;
        }

        public async Task<GameType[]> GetAllGameTypesAsync()
        {
            IQueryable<GameType> query = _context.GameTypes;

            query = query.OrderByDescending(g => g.Name);

            return await query.ToArrayAsync();
        }

        public async Task<GameType> GetGameTypeAsync(int gameTypeId)
        {
            IQueryable<GameType> query = _context.GameTypes
                .Where(g => g.GameTypeId == gameTypeId);

            return await query.FirstOrDefaultAsync();
        }
    }
}
