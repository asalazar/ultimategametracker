﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimateGameTracker.Data.Entities;

namespace UltimateGameTracker.Data
{
    public class LocationRepository : BaseRepository, ILocationRepository
    {
        private readonly UltimateGameTrackerContext _context;

        public LocationRepository(UltimateGameTrackerContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Location[]> GetAllLocationsAsync()
        {
            IQueryable<Location> query = _context.Locations;

            return await query.ToArrayAsync();
        }

        public async Task<Location> GetLocationAsync(int locationId)
        {
            IQueryable<Location> query = _context.Locations
                .Where(e => e.LocationId == locationId);

            return await query.FirstOrDefaultAsync();
        }
    }
}
