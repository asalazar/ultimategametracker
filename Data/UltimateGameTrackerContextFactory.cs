﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace UltimateGameTracker.Data
{
    public class UltimateGameTrackerContextFactory : IDesignTimeDbContextFactory<UltimateGameTrackerContext>
    {
        public UltimateGameTrackerContext CreateDbContext(string[] args)
        {
            var config = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json")
              .Build();

            return new UltimateGameTrackerContext(new DbContextOptionsBuilder<UltimateGameTrackerContext>().Options, config);
        }
    }
}
