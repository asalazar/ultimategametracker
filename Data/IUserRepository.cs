﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UltimateGameTracker.Data.Entities;

namespace UltimateGameTracker.Data
{
    public interface IUserRepository
    {
        // General 
        void Add<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveChangesAsync();

        Task<User> GetByUsernameAndPassword(string username, string password);
        Task<User[]> GetAllUsersAsync();
        Task<User> GetUserAsync(int userId);
    }
}
