﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimateGameTracker.Data.Entities;

namespace UltimateGameTracker.Data
{
    public class GameRepository : BaseRepository, IGameRepository
    {
        private readonly UltimateGameTrackerContext _context;

        public GameRepository(UltimateGameTrackerContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Game[]> GetAllGamesAsync()
        {
            IQueryable<Game> query = _context.Games
                .Include(g => g.GameType)
                .Include(g => g.Event).ThenInclude(e => e.Location)
                .Include(g => g.UserGames).ThenInclude(ug => ug.User); // many to many

            query = query.OrderByDescending(g => g.Name);

            return await query.ToArrayAsync();
        }

        public async Task<Game> GetGameAsync(int GameId)
        {
            IQueryable<Game> query = _context.Games
                .Where(g => g.GameId == GameId)
                .Include(g => g.GameType)
                .Include(g => g.Event).ThenInclude(e => e.Location)
                .Include(g => g.UserGames).ThenInclude(ug => ug.User); // many to many

            return await query.FirstOrDefaultAsync();
        }
    }
}
