﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UltimateGameTracker.Data.Entities;

namespace UltimateGameTracker.Data.Entities
{
    public class Event
    {
        public int EventId { get; set; }
        public string Name { get; set; }
        public bool IsReoccuring { get; set; }
        public DateTime DateTime { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public ICollection<Game> Games { get; set; }
        public Location Location { get; set; }
    }
}
