﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UltimateGameTracker.Data.Entities;

namespace UltimateGameTracker.Data.Entities
{
    public class Game
    {
        public int GameId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public GameType GameType { get; set; }
        public Event Event { get; set; }

        public ICollection<UserGame> UserGames { get; set; }
    }
}
