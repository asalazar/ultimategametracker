﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UltimateGameTracker.Data.Entities
{
    public class GameType
    {
        public int GameTypeId { get; set; }
        public string Name { get; set; }
        public ICollection<Game> Games { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
