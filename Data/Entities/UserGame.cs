﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UltimateGameTracker.Data.Entities
{
    public class UserGame
    {
            public int UserId { get; set; }
            public User User { get; set; }
            public int GameId { get; set; }
            public Game Game { get; set; }
    }
}
