﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UltimateGameTracker.Data.Entities;
using UltimateGameTracker.Models;

namespace UltimateGameTracker.Data
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        private readonly UltimateGameTrackerContext _context;

        public UserRepository(UltimateGameTrackerContext context) : base(context)
        {
            _context = context;
        }

        public async Task<User> GetByUsernameAndPassword(string username, string password)
        {
            IQueryable<User> userQuery = _context.Users
                .Where(u => u.UserName == username && u.Password == password.Sha256());

            return await userQuery.FirstOrDefaultAsync();
        }

        public async Task<User[]> GetAllUsersAsync()
        {
            IQueryable<User> query = _context.Users;

            query = query.OrderByDescending(c => c.LastName);

            return await query.ToArrayAsync();
        }

        public async Task<User> GetUserAsync(int userId)
        {
            IQueryable<User> query = _context.Users
                .Where(e => e.UserId == userId);

            return await query.FirstOrDefaultAsync();
        }
    }
}
