﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using UltimateGameTracker.Data.Entities;
using UltimateGameTracker.Models;

namespace UltimateGameTracker.Data
{
    public class UltimateGameTrackerProfile : Profile
    {
        public UltimateGameTrackerProfile()
        {
            this.CreateMap<User, UserModel>()
                .ReverseMap();

            this.CreateMap<Event, EventModel>()
                .ReverseMap();

            this.CreateMap<Game, GameModel>()
                .ForMember(g => g.Players, opt => opt.MapFrom(x => x.UserGames.Select(y => y.User))) // many to many
                .ReverseMap();

            this.CreateMap<GameType, GameTypeModel>()
                .ReverseMap();

            this.CreateMap<Location, LocationModel>()
                .ReverseMap();

            this.CreateMap<Event, GameEventModel>()
                .ReverseMap();

            this.CreateMap<Game, EventGameModel>()
                .ReverseMap();
        }
    }
}
