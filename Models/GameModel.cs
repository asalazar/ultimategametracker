﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimateGameTracker.Data.Entities;

namespace UltimateGameTracker.Models
{
    public class GameModel
    {
        [Required]
        public int GameId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public GameTypeModel GameType { get; set; }
        public GameEventModel Event { get; set; }
        public ICollection<UserModel> Players { get; set; }
    }
}