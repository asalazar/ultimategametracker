﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UltimateGameTracker.Data.Entities;

namespace UltimateGameTracker.Models
{
    public class EventModel
    {
        [Required]
        public int EventId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public bool IsReoccuring { get; set; }
        [Required]
        public DateTime DateTime { get; set; }
        public ICollection<EventGameModel> Games { get; set; }
        public LocationModel Location { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
