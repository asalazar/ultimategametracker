﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UltimateGameTracker.Models
{
    public class GameEventModel
    {
        public int EventId { get; set; }
        public string Name { get; set; }
        public bool IsReoccuring { get; set; }
        public DateTime DateTime { get; set; }
        public LocationModel Location { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
