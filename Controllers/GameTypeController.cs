﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using UltimateGameTracker.Data;
using UltimateGameTracker.Data.Entities;
using UltimateGameTracker.Models;

namespace UltimateGameTracker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameTypesController : ControllerBase
    {
        private readonly IGameTypeRepository _repository;
        private readonly IMapper _mapper;
        private readonly LinkGenerator _linkGenerator;

        public GameTypesController(IGameTypeRepository repository, IMapper mapper, LinkGenerator linkGenerator)
        {
            _repository = repository;
            _mapper = mapper;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<GameTypeModel[]>> Get()
        {
            try
            {
                var gameTypes = await _repository.GetAllGameTypesAsync();
                return _mapper.Map<GameTypeModel[]>(gameTypes);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }
        }

        [HttpPost]
        public async Task<ActionResult<GameTypeModel>> Post(GameTypeModel model)
        {
            try
            {
                var gameType = _mapper.Map<GameType>(model);
                _repository.Add(gameType);

                if(await _repository.SaveChangesAsync())
                {
                    return Created("", _mapper.Map<GameTypeModel>(gameType));
                }
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }

            return BadRequest();
        }

        [HttpPut("{gameTypeId:int}")]
        public async Task<ActionResult<GameTypeModel>> Put(int gameTypeId, GameTypeModel model)
        {
            try
            {
                var oldGameType = await _repository.GetGameTypeAsync(gameTypeId);
                if (oldGameType == null) return NotFound($"Could not find GameType with ID of {gameTypeId}");

                _mapper.Map(model, oldGameType);

                if (await _repository.SaveChangesAsync())
                {
                    return _mapper.Map<GameTypeModel>(oldGameType);
                }
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e);
            }

            return BadRequest();
        }

        [HttpDelete("{gameTypeId:int}")]
        public async Task<IActionResult> Delete(int gameTypeId)
        {
            try
            {
                var gameType = await _repository.GetGameTypeAsync(gameTypeId);
                if (gameType == null) return NotFound("Failed to find the GameType to delete");
                _repository.Delete(gameType);

                if (await _repository.SaveChangesAsync())
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("Failed to delete GameType");
                }
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }
        }
    }
}
