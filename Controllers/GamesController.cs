﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using UltimateGameTracker.Data;
using UltimateGameTracker.Data.Entities;
using UltimateGameTracker.Models;

namespace UltimateGameTracker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GamesController : ControllerBase
    {
        private readonly IGameRepository _repository;
        private readonly IMapper _mapper;
        private readonly LinkGenerator _linkGenerator;

        public GamesController(IGameRepository repository, IMapper mapper, LinkGenerator linkGenerator)
        {
            _repository = repository;
            _mapper = mapper;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<GameModel[]>> Get()
        {
            try
            {
                var games = await _repository.GetAllGamesAsync();
                return _mapper.Map<GameModel[]>(games);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }
        }

        [HttpGet("{gameId:int}")]
        public async Task<ActionResult<GameModel>> GetGame(int gameId)
        {
            try
            {
                var game = await _repository.GetGameAsync(gameId);
                return _mapper.Map<GameModel>(game);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }
        }

        [HttpPost]
        public async Task<ActionResult<GameModel>> Post(GameModel model)
        {
            try
            {
                var game = _mapper.Map<Game>(model);
                _repository.Add(game);

                if(await _repository.SaveChangesAsync())
                {
                    return Created("", _mapper.Map<GameModel>(game));
                }
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }

            return BadRequest();
        }

        [HttpPut("{gameId:int}")]
        public async Task<ActionResult<GameModel>> Put(int gameId, GameModel model)
        {
            try
            {
                var oldGame = await _repository.GetGameAsync(gameId);
                if (oldGame == null) return NotFound($"Could not find Game with ID of {gameId}");

                _mapper.Map(model, oldGame);

                if (await _repository.SaveChangesAsync())
                {
                    return _mapper.Map<GameModel>(oldGame);
                }
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e);
            }

            return BadRequest();
        }

        [HttpDelete("{gameId:int}")]
        public async Task<IActionResult> Delete(int gameId)
        {
            try
            {
                var game = await _repository.GetGameAsync(gameId);
                if (game == null) return NotFound("Failed to find the Game to delete");
                _repository.Delete(game);

                if (await _repository.SaveChangesAsync())
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("Failed to delete Game");
                }
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }
        }
    }
}
