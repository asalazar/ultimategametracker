﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using UltimateGameTracker.Data;
using UltimateGameTracker.Data.Entities;
using UltimateGameTracker.Models;

namespace UltimateGameTracker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _repository;
        private readonly IMapper _mapper;
        private readonly LinkGenerator _linkGenerator;

        public UsersController(IUserRepository repository, IMapper mapper, LinkGenerator linkGenerator)
        {
            _repository = repository;
            _mapper = mapper;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<UserModel[]>> Get()
        {
            try
            {
                var users = await _repository.GetAllUsersAsync();
                return _mapper.Map<UserModel[]>(users);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }
        }

        [HttpGet("{userId:int}")]
        public async Task<ActionResult<UserModel>> Get(int userId)
        {
            try
            {
                var user = await _repository.GetUserAsync(userId);
                return _mapper.Map<UserModel>(user);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }
        }

        [HttpPost]
        public async Task<ActionResult<UserModel>> Post(UserModel model)
        {
            try
            {
                var user = _mapper.Map<User>(model);
                _repository.Add(user);

                if (await _repository.SaveChangesAsync())
                {
                    return Created("", _mapper.Map<UserModel>(user));
                }
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }

            return BadRequest();
        }

        [HttpPut("{userId:int}")]
        public async Task<ActionResult<UserModel>> Put(int userId, UserModel model)
        {
            try
            {
                var oldUser = await _repository.GetUserAsync(userId);
                if (oldUser == null) return NotFound($"Could not find User with ID of {userId}");

                _mapper.Map(model, oldUser);

                if (await _repository.SaveChangesAsync())
                {
                    return _mapper.Map<UserModel>(oldUser);
                }
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e);
            }

            return BadRequest();
        }

        [HttpDelete("{userId:int}")]
        public async Task<IActionResult> Delete(int userId)
        {
            try
            {
                var user = await _repository.GetUserAsync(userId);
                if (user == null) return NotFound("Failed to find the User to delete");
                _repository.Delete(user);

                if (await _repository.SaveChangesAsync())
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("Failed to delete User");
                }
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }
        }
    }
}
