﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using UltimateGameTracker.Data;
using UltimateGameTracker.Data.Entities;
using UltimateGameTracker.Models;

namespace UltimateGameTracker.Controllers
{
    // SECURITY: "[Authorize]" on the controller ensures that nothing in this controller can be
    // accessed without being authenticated. This can also be used on individual methods to secure
    // just those methods. This checks the default scheme which is cookie authentiction. To check a
    // different scheme use "Authorize(<scheme>)". The default is unsecured. I've actively set all
    // controllers to default to secured so we don't need this decorator attribute here. Instead,
    // since the default is secured, we need to use "[AllowAnonymous]" to open any end points that
    // do not need to be secured.
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly IEventRepository _repository;
        private readonly IMapper _mapper;
        private readonly LinkGenerator _linkGenerator;

        public EventsController(IEventRepository repository, IMapper mapper, LinkGenerator linkGenerator)
        {
            _repository = repository;
            _mapper = mapper;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<EventModel[]>> Get()
        {
            try
            {
                var events = await _repository.GetAllEventsAsync();
                return _mapper.Map<EventModel[]>(events);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }
        }

        [HttpGet("{date}")]
        public async Task<ActionResult<EventModel[]>> Get(DateTime date)
        {
            try
            {
                var events = await _repository.GetAllEventsByDate(date);
                return _mapper.Map<EventModel[]>(events);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }
        }

        [HttpGet("{eventId:int}")]
        public async Task<ActionResult<EventModel>> Get(int eventId)
        {
            try
            {
                var evnt = await _repository.GetEventAsync(eventId);
                return _mapper.Map<EventModel>(evnt);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }
        }

        [HttpPost]
        public async Task<ActionResult<EventModel>> Post(EventModel model)
        {
            try
            {
                var evnt = _mapper.Map<Event>(model);
                _repository.Add(evnt);

                if(await _repository.SaveChangesAsync())
                {
                    return Created("", _mapper.Map<EventModel>(evnt));
                }
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }

            return BadRequest();
        }

        [HttpPut("{eventId:int}")]
        public async Task<ActionResult<EventModel>> Put(int eventId, EventModel model)
        {
            try
            {
                var oldEvent = await _repository.GetEventAsync(eventId);
                if (oldEvent == null) return NotFound($"Could not find Event with ID of {eventId}");

                _mapper.Map(model, oldEvent);

                if (await _repository.SaveChangesAsync())
                {
                    return _mapper.Map<EventModel>(oldEvent);
                }
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e);
            }

            return BadRequest();
        }

        [HttpDelete("{eventId:int}")]
        public async Task<IActionResult> Delete(int eventId)
        {
            try
            {
                var evnt = await _repository.GetEventAsync(eventId);
                if (evnt == null) return NotFound("Failed to find the Event to delete");
                _repository.Delete(evnt);

                if (await _repository.SaveChangesAsync())
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("Failed to delete Event");
                }
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }
        }
    }
}
