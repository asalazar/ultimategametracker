﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using UltimateGameTracker.Data;
using UltimateGameTracker.Data.Entities;
using UltimateGameTracker.Models;

namespace UltimateGameTracker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationsController : ControllerBase
    {
        private readonly ILocationRepository _repository;
        private readonly IMapper _mapper;
        private readonly LinkGenerator _linkGenerator;

        public LocationsController(ILocationRepository repository, IMapper mapper, LinkGenerator linkGenerator)
        {
            _repository = repository;
            _mapper = mapper;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<LocationModel[]>> Get()
        {
            try
            {
                var locations = await _repository.GetAllLocationsAsync();
                return _mapper.Map<LocationModel[]>(locations);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }
        }

        [HttpGet("{locationId:int}")]
        public async Task<ActionResult<LocationModel>> Get(int locationId)
        {
            try
            {
                var location = await _repository.GetLocationAsync(locationId);
                return _mapper.Map<LocationModel>(location);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }
        }

        [HttpPost]
        public async Task<ActionResult<LocationModel>> Post(LocationModel model)
        {
            try
            {
                var location = _mapper.Map<Location>(model);
                _repository.Add(location);

                if(await _repository.SaveChangesAsync())
                {
                    return Created("", _mapper.Map<LocationModel>(location));
                }
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }

            return BadRequest();
        }

        [HttpPut("{locationId:int}")]
        public async Task<ActionResult<LocationModel>> Put(int locationId, LocationModel model)
        {
            try
            {
                var oldLocation = await _repository.GetLocationAsync(locationId);
                if (oldLocation == null) return NotFound($"Could not find Location with ID of {locationId}");

                _mapper.Map(model, oldLocation);

                if (await _repository.SaveChangesAsync())
                {
                    return _mapper.Map<LocationModel>(oldLocation);
                }
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e);
            }

            return BadRequest();
        }

        [HttpDelete("{locationId:int}")]
        public async Task<IActionResult> Delete(int locationId)
        {
            try
            {
                var location = await _repository.GetLocationAsync(locationId);
                if (location == null) return NotFound("Failed to find the Location to delete");
                _repository.Delete(location);

                if (await _repository.SaveChangesAsync())
                {
                    return Ok();
                }
                else
                {
                    return BadRequest("Failed to delete Location");
                }
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Database Failure: " + e);
            }
        }
    }
}
