using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Microsoft.CodeAnalysis;
using AutoMapper;
using UltimateGameTracker.Data;
using System.Reflection;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Http;

namespace UltimateGameTracker
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<UltimateGameTrackerContext>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IEventRepository, EventRepository>();
            services.AddScoped<IGameRepository, GameRepository>();
            services.AddScoped<ILocationRepository, LocationRepository>();
            services.AddScoped<IGameTypeRepository, GameTypeRepository>();

            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            // SECURITY: This adds security to all controllers by default so that "[Authorize]"
            // doesn't have to be added to each controller.
            services.AddControllers(o => o.Filters.Add(new AuthorizeFilter()));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(name: "v1", new OpenApiInfo { Title = "UltimateGameTracker API", Version = "v1" });
            });

            // SECURITY: This adds an authentiction scheme to the configuration
            // There are many schemes but in this case I'm using cookie authentication.
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.ExpireTimeSpan = TimeSpan.FromDays(30);
                    options.SlidingExpiration = true;
                    options.Cookie.SameSite = SameSiteMode.None;
                    options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                    //options.Cookie.IsEssential = true;
                    options.Events.OnRedirectToLogin = (context) => // add this option to return a "401 Unauthorized" instead of the default "302 Redirect"
                    {
                        context.Response.StatusCode = 401;
                        return Task.CompletedTask;
                    };
                });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, UltimateGameTrackerContext ultimateGameTrackerContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(url: "/swagger/v1/swagger.json", name: "UltimateGameTracker API V1");
            });

            app.UseRouting();

            // SECURITY: Never put this after "UseEndpoints()" because then authentication will
            // happen after the requests end
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
